SHELL = /bin/bash
PYTHON_BIN = ./venv/bin/python


dependencies:
	source ./venv/bin/activate
	pip freeze | tee requirements.txt

setup:
	python3 -m venv ./venv
	source ./venv/bin/activate
	pip install wheel
	pip install -r requirements.txt

test:
	source ./venv/bin/activate
	python -m pytest -vv --cov=./compiler

watch-test:
	source ./venv/bin/activate
	ptw ./tests

clean:
	find ./server -name "__pycache__" | xargs rm -rf {}
	find -iname "*.pyc" -delete
