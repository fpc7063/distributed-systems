import os
from dotenv import load_dotenv
from nodes.node import MainNode, BackupNode


def load_treat_env():
    """
    Treat any faulting environment variables
    """
    load_dotenv()
    environment = {}

    host = os.environ.get('HOST')
    environment['host'] = host if host is not None else "localhost"

    port = os.environ.get('PORT')
    environment['port'] = port if port is not None else "9999"

    node_type = os.environ.get('NODE_TYPE')
    environment['node_type'] = node_type if node_type is not None else "main"

    environment['main_socket'] = os.environ.get('MAIN_SOCKET')
    environment['main_port'] = os.environ.get('MAIN_PORT')
    return environment


if __name__ == "__main__":
    print("executing main")
    environment = load_treat_env()

    if(environment['node_type'] == "main"):
        server = MainNode(HOST=environment['host'], PORT=environment['port'])
        server.start()
    elif(environment['node_type'] == "backup"):
        server = BackupNode(HOST=environment['host'], PORT=environment['port'],
                    MAIN_SOCKET=environment['main_socket'], MAIN_PORT=environment['main_port'])
        server.start()
