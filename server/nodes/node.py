"""
Node class & utils Module
"""
import socket
import logging
import sys

from .socket_server.server import Server


node_logger = logging.getLogger('NodeLogger')
class MainNode(Server):
    """
    Main server node. This will receive connections from other nodes
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        node_logger.debug('MainNode instanciated %s', id(self))

    def start(self):
        self.socket_server.serve_forever()
    
    def _signal_handler_termination(self, signum, frame):
        """
        Handles SIGINIT, does clean up on server
        """
        self.socket_server.server_close()
        sys.exit(0)

class BackupNode(Server):
    """
    BackupNode server node. This will receive connections from other nodes
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.socket_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.main_socket = kwargs['MAIN_SOCKET']
        self.main_port = kwargs['MAIN_PORT']
        node_logger.debug('BackupNode instanciated %s', id(self))

    def start(self):
        self.socket_server.serve_forever()
        self.socket_client.connect(self.main_socket, self.main_port)
    
    def _signal_handler_termination(self, signum, frame):
        """
        Handles SIGINIT, does clean up on server
        """
        self.socket_server.server_close()
        self.socket_client.shutdown()
        sys.exit(0)
