import socketserver
import signal
import sys
import abc


class ServerTCPHandler(socketserver.BaseRequestHandler):
    """
    TCP Server Handler
    """
    def setup(self):
        """
        Does something BEFORE self.handle()
        """

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        print(f"{self.client_address[0]} wrote:")
        print(self.data)
        # just send back the same data, but upper-cased
        self.request.sendall(self.data.upper())

    def finish(self):
        """
        Does something AFTER self.handle()
        """


class Server():
    """
    Server abstraction
    """
    def __init__(self, *args, **kwargs):
        signal.signal(signal.SIGINT, self._signal_handler_termination)
        signal.signal(signal.SIGTERM, self._signal_handler_termination)
        signal.signal(signal.SIGHUP, self._signal_handler_termination)
        self.socket_server = socketserver.TCPServer((kwargs['HOST'], int(kwargs['PORT'])), ServerTCPHandler)

    @abc.abstractmethod
    def _signal_handler_termination(self, signum, frame):
        pass

    @abc.abstractmethod
    def start(self):
        pass
