FROM debian:bullseye

LABEL maintainer="fpc7063" contact="francisco.pc7063@gmail.com"

ENV DEBIAN_FRONTEND noninteractive


# Update dependencies
RUN apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y python3 python3-venv python3-pip \
&& apt-get install -y locales iproute2 procps \
&& apt-get install -y nodejs npm \
&& npm install -g nodemon \
&& rm -rf /var/lib/apt/lists/*

# CONFIG FOR Image Development
ADD development.sh /tmp/development.sh
RUN chmod +x /tmp/development.sh

# ENV SETUP
WORKDIR /opt
COPY . .
RUN python3 -m venv ./venv \
    && pip install -r requirements.txt

# ENVIRONMENT VARIABLES
RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8
ENV TZ America/Sao_Paulo


#ENTRYPOINT ["bash", "-c"]
ENTRYPOINT [ "python3", "-u" ]
CMD [ "server/main.py" ]